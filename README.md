# OpenML dataset: BraidFlow

https://www.openml.org/d/45966

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Entering a cognitive state of flow is a natural response of the mind that allows people to fully concentrate and cope with tedious, and often repetitive tasks. Understanding how to trigger or sustain flow remains limited by retrospective surveys, presenting a need to better document flow. This dataset is used to study flow in the context of a flow-inducing task, i.e., braidmaking. Using an instrumented Kumihimo braidmaking tool and off-the-shelf biosignal wristbands, we record the experiences of 24 users engaged in 3 different braidmaking tasks. We extract feature motivated from flow literature from activity data (IMU, EMG, EDA, heart rate, skin temperature, braiding telemetry) and annotated with Flow Short Scale (FSS) scores.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45966) of an [OpenML dataset](https://www.openml.org/d/45966). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45966/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45966/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45966/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

